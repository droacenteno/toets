<?php
/**
 * Contact.php - renders a contact form
 * 
 * @author Bugslayer
 * 
 */
?>
<form name="input" action="?action=save&page=contact" method="post"
	style="width: 850px; margin-left: auto; margin-right: auto">
	<h1>Contact</h1>
	<p>Vul dit formulier in om een bericht te sturen:</p>
	<table style="width:850px">
		<tr>
			<td width="230px"><label for="Name_first">Voornaam *</label></td>
			<td width="265px"><input type="text" id="name_first"
				name="Name_first" maxlength="50" size="30"></td>
			<td><span id="Name_firstValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Name_middle">Tussenvoegsel</label></td>
			<td><input type="text" id="Name_middle" name="Name_middle" maxlength="50"
				size="30"></td>
			<td><span id="Name_middleValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Name_last">Achternaam *</label></td>
			<td><input type="text" id="Name_last" name="Name_last" maxlength="50"
				size="30"></td>
			<td><span id="Name_lastValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Email">Email Adres *</label></td>
			<td><input type="text" id="email" name="Email" maxlength="80"
				size="30"></td>
			<td><span id="EmailValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Telephone">Telefoonnummer</label></td>
			<td><input type="text" id="Telephone" name="Telephone" maxlength="50"
				size="30"></td>
			<td><span id="TelephoneValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Address_street">Adres straat</label></td>
			<td><input type="text" id="Address_street" name="Address_street"
				maxlength="50" size="30"></td>
			<td><span id="Address_streetValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Address_number">Huisnummer</label></td>
			<td><input type="text" id="Address_number" name="Address_number"
				maxlength="50" size="30"></td>
			<td><span id="Address_numberValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Address_zipcode">Adres postcode</label></td>
			<td><input type="text" id="Address_zipcode" name="Address_zipcode"
				maxlength="50" size="30"></td>
			<td><span id="Address_zipcodeValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Address_city">Adres plaatsnaam</label></td>
			<td><input type="text" id="Address_city" name="Address_city"
				maxlength="50" size="30"></td>
			<td><span id="Address_cityValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Subject">Onderwerp *</label></td>
			<td><input type="text" id="Subject" name="Subject" maxlength="30"
				size="30"></td>
			<td><span id="SubjectValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Message">Bericht *</label></td>
			<td><textarea id="Message" name="Message" maxlength="1000" cols="25"
					rows="6"></textarea></td>
			<td><span id="MessageValResult"> </span></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center"><input type="submit"
				value="Verstuur"></td>
		</tr>
	</table>
</form>
<!-- Het javascript bestand voor de validatie laden -->
<script src="js/contact.js"></script>
