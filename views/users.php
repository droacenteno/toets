<?php 
/**
 * Users.php - renders, well, it should render a list of users
 * 
 * @author Bugslayer
 * 
 */
 
// Check if the request is done by an authorized user. If not, show 401.php and exit
if (!isAuthenticated()) {
	include '401.php';
	exit();
}

?>
<h1>Gebruikers</h1>
<table border="1">
	<th>
		
		<td>UserID</td>
		<td>Naam</td>
		<td>Mail</td>
	</th>
	<tb>
		<tr>
			<td>1</td>
			<td>pas</td>
			<td>Daan Beckers</td>
			<td><a href="mailto:d.beckers@hz.nl?Subject=contact" target="_top">Send Mail</a></td>
		</tr>	
		<tr>
			<td>2</td>
			<td>hjk</td>
			<td>Bouke Bil</td>
			<td><a href="mailto:b.bil@hz.nl?Subject=contact" target="_top">Send Mail</a></td>
		</tr>	
		<tr>
			<td>3</td>
			<td>dfg</td>
			<td>Mischa de Boer</td>
			<td><a href="mailto:m.de.boer@hz.nl?Subject=contact" target="_top">Send Mail</a></td>
		</tr>	
		<tr>
			<td>4</td>
			<td>qwe</td>
			<td>Anton Everse</td>
			<td><a href="mailto:a.everse@hz.nl?Subject=contact" target="_top">Send Mail</a></td>
		</tr>
		<tr>
			<td>5</td>
			<td>uio</td>
			<td>Wouter de Vijlder</td>
			<td><a href="mailto:w.de.vijlder@hz.nl?Subject=contact" target="_top">Send Mail</a></td>
		</tr>	
		<tr>
			<td>6</td>
			<td>admin</td>
			<td>Daan de Waard</td>
			<td><a href="mailto:daan.de.waard@hz.nl?Subject=contact" target="_top">Send Mail</a></td>
		</tr>	
		<tr>
			<td>7</td>
			<td>rty</td>
			<td>Ruth de Waard</td>
			<td><a href="mailto:r.de.waard@hz.nl?Subject=contact" target="_top">Send Mail</a></td>
		</tr>	
		
</tb>
</table>
