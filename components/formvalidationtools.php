<?php
/**
 * Formvalidationtools.php - Handy functions for validating data. All these functions return a txt message is the input 
 * does nnot match the validation requirements or an empty string if the input does match the requirements.
 * 
 * @author Bugslayer
 * 
 */

/**
 * Validates if a text has a minimum length.
 *
 * @param $text input
 *        	to be validated
 * @param $minSize minimum
 *        	required length
 * @param $errorMsg the
 *        	message that should be returned
 *        	
 * @return the $errorMsg if $text does not match the requirement, otherwise an empty string.
 */
function validateLength($text, $minSize, $errorMsg) {
	$result = "";
	if (strlen ( $text ) < $minSize) {
		$result = $errorMsg . "<br/>";
	}
	return $result;
}

/**
 * Validates if a text has characters (a-z, A-Z).
 *
 * @param $text input
 *        	to be validated
 * @param $errorMsg the
 *        	message that should be returned
 *        	
 * @return the $errorMsg if $text does not match the requirement, otherwise an empty string.
 */
function validateCharacters($text, $errorMsg) {
	$regex_pattern = "/^[A-Za-z .'-]+$/";
	return validateRegex ( $regex_pattern, $text, $errorMsg );
}

/**
 * Validates if a variable is a number or a numeric string.
 *
 * @param $text input
 *        	to be validated
 * @param $errorMsg the
 *        	message that should be returned
 *        	
 * @return the $errorMsg if $text does not match the requirement, otherwise an empty string.
 */
function validateNumber($text, $errorMsg) {
	$result = "";
	if (! is_numeric ( $text )) {
		$result = $errorMsg . "<br/>";
	}
	return $result;
}

/**
 * Validates if a variable is an email address.
 *
 * @param $text input
 *        	to be validated
 * @param $errorMsg the
 *        	message that should be returned
 *        	
 * @return the $errorMsg if $text does not match the requirement, otherwise an empty string.
 */
function validateEmail($text, $errorMsg) {
	$regex_pattern = "/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/";
	return validateRegex ( $regex_pattern, $text, $errorMsg );
}

/**
 * Validates if a variable meets a specified regex.
 *
 * @param $regex_pattern the
 *        	regex pattern to test
 * @param $text input
 *        	to be validated
 * @param $errorMsg the
 *        	message that should be returned
 *        	
 * @return the $errorMsg if $text does not match the requirement, otherwise an empty string.
 */
function validateRegex($regex_pattern, $text, $errorMsg) {
	$result = "";
	if (! preg_match ( $regex_pattern, $text )) {
		$result = $errorMsg . "<br/>";
	}
	return $result;
}

?>